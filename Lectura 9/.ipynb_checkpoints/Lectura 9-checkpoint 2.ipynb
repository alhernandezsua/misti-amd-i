{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lectura 9: Entrenamiento de Algoritmos de Aprendizaje Máquina \n",
    "\n",
    "## Aplicaciones de Minería de Datos I\n",
    "### Mayo, 2023"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# El cerebro como diseño principal de inteligencia artificial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "En 1943 John Smith et al. crearon el concepto de **célula del cerebro simplificada**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![title](Lectura9-1.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Las neuronas están interconectadas en el cerebro e involucran el **procesamiento** y **transmisión** de señales eléctricas y químicas \t\t\n",
    "2. La neurona  es como una compuerta lógica con **salidas binarias**\n",
    "3. Múltiples señales llegan a la **dentrita**, después se integran al cuerpo celular, si la señal acumulada excede un **umbral**, la señal de salida es transmitida por el **axón**\n",
    "\n",
    "En 1957 Rosenblatt, F. publicó el concepto de **regla de aprendizaje del Perceptrón**. Un algoritmo que aprende de los **coeficientes de peso** más óptimos multiplicados por las **características de entrada** (señales) para tomar una **decisión**. En el contexto de aprendizaje supervisado el algoritmo se resume a : *predecir si una muestra pertenece a una clase o no*\n",
    "\n",
    "![title](Lectura9-2.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Definición formal del Perceptrón\n",
    "\n",
    "\n",
    "1.\tSe define como un problema de clasificación binaria $1$ o $0$\n",
    "2.\tSe define una función de activación $\\phi(z)$\n",
    "3.\tSe tiene una combinación lineal de ciertos valores de entrada $\\mathbf{x}$ y un correspondiente vector de pesos $\\mathbf{w}$, donde $z$ es conocida como la **entrada a la red** ($z = w_{1}x_{1}+ \\dots + w_{m}x_{m}$):\n",
    "\n",
    "\\begin{equation}\n",
    " \\begin{bmatrix} \n",
    "    w_{1} \\\\\n",
    "    \\vdots \\\\\n",
    "\tw_{m} \\\\\n",
    "    \\end{bmatrix},\n",
    "    x = \n",
    " \\begin{bmatrix} \n",
    "    x_{1} \\\\\n",
    "    \\vdots \\\\\n",
    "\tx_{m} \\\\\n",
    "    \\end{bmatrix}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "4. Si la activación de una muestra en particular $x^{(i)}$, esto es, la salida $\\phi(z)$, es mayor que un umbral definido $\\theta$, el resultado predicho es  $1$, de otra forma $-1$\n",
    "5. La función de activación es una  *función de escalón unitario*:\n",
    "\\begin{equation}\n",
    "\\begin{cases}\n",
    "1, \\text{si z}  \\geq \\theta\\\\\n",
    "-1,    \\text{lo demás}    \n",
    "\\end{cases}\n",
    "\\end{equation}\n",
    "6. Si se despeja el umbral y se define un peso  cero como $w_0=-\\theta$ y $x_0=1$, se puede escribir $z$ de una manera más compacta :\n",
    "\\begin{equation}\n",
    "z = w_{0}x_{0} + w_{1}x_{1} + \\dots + w_{m}x_{m} = \\mathbf{w}^T \\mathbf{x}\n",
    "\\end{equation}\n",
    "\\begin{equation}\n",
    "z = \\sum_{j=0}^{m} \\mathbf{x_j}\\mathbf{w_j}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La idea principal de la neurona de MCP y el Perceptrón con umbral de Rosenblatt es utilizar un alcance reducido para emular una neurona individual tal como el cerebro funciona : **dispara una secuencia o no** . En la  siguiente Fig a. se muestra como la entrada a la red es confinada a una salida binaria y en la Fig b. cómo se puede discriminar entre dos clases separables.\n",
    "\n",
    "![title](Lectura9-3.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Regla de actualización de pesos perceptrón\n",
    "\n",
    "1. Iniciar los pesos $\\mathbf{w}$ a $0$ o a números reales pequeños\n",
    "2. Para cada muestra de entrenamiento $\\mathbf{x}^{(i)}$ seguir los siguientes pasos:\n",
    "\n",
    "    a. Calcular el valor de salida $\\hat{y}$\n",
    "\n",
    "    b. Actualizar los pesos\n",
    "3.\tEl valor de salida es la clase predicha por la función de escalón unitario\n",
    "4.\tLa actualización simultanea de cada peso $w_{j}$ en el vector de pesos $\\mathbf{w}$  se escribe como:\n",
    "\n",
    "\\begin{equation}\n",
    "w_{j} := w_{j} + \\Delta w_{j}\n",
    "\\end{equation}\n",
    "5. El valor $\\Delta w_{j}$ que es utilizado para actualizar el peso $w_{j}$, es calculado por la regla de aprendizaje del perceptrón:\n",
    "\n",
    "\\begin{equation}\n",
    "\\Delta w_{j} = \\eta (y^{(i)}-\\hat{y}^{(i)})\n",
    "\\end{equation}\n",
    "\n",
    "En donde:\n",
    "\n",
    "1.\t$\\eta$ es el radio de aprendizaje (constante ente $[0,1]$)\n",
    "2.\t$y^{(i)}$ es la clase verdadera de la \t**i-ésima**  muestra\n",
    "3.\t$\\hat{y}^{(i)}$ es la clase predicha\n",
    "\n",
    "Para un conjunto de datos en  $\\mathbb{R}^2$ se puede realizar el cálculo de actualizaciones de la siguiente manera:\n",
    "\n",
    "1.\t $\\Delta w_{0} = \\eta (y^{(i)}-\\text{salida}^{(i)})$\n",
    "2.\t $\\Delta w_{1} = \\eta (y^{(i)}-\\text{salida}^{(i)})x_1$\n",
    "3.\t $\\Delta w_{2} = \\eta (y^{(i)}-\\text{salida}^{(i)})x_2$\n",
    "\n",
    "Por ejemplo, si el Perceptrón predice la clase correctamente, los pesos permanecen intactos:\n",
    "\n",
    "\\begin{equation}\n",
    "\\Delta w_{j} = \\eta (-1--1)x_{j}^{(j)} = 0 \n",
    "\\end{equation}\n",
    "\n",
    "\\begin{equation}\n",
    "\\Delta w_{j} = \\eta (1-1)x_{j}^{(j)} = 0\n",
    "\\end{equation}\n",
    "\n",
    "La convergencia del Perceptrón está garantizada si se cumplen los siguientes puntos:\n",
    "\n",
    "La dos clases son **linealmente separables**\n",
    "1. El radio de aprendizaje es lo suficientemente pequeño\n",
    "2. Si las dos clases no pueden ser separadas por una frontera de decisión se definen un máximo número de rondas en el conjunto de pruebas $X_{t}$ o un umbral para las clases **mal clasificadas**.\n",
    "\n",
    "En la  siguiente Fig. se muestra un ejemplo de datos linealmente separables y aquellos que no\n",
    "\n",
    "![title](Lectura9-4.png)\n",
    "\n",
    "En la siguiente Fig. se ilustra como el Perceptrón recibe las entradas de una muestra $\\mathbf{x}$ y las combina con sus respectivos pesos $\\mathbf{w}$. La entrada a la red pasa a la función de activación  (función de escalón unitario) y genera la salida binaria:\n",
    "\n",
    "![title](Lectura9-5.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import Perceptron\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.feature_extraction.text import TfidfVectorizer\n",
    "from sklearn.pipeline import Pipeline\n",
    "from sklearn.metrics import accuracy_score\n",
    "import pandas as pd\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = np.array([[1,0,0,0],[1,0,1,0],[1,1,0,0],[1,1,1,1]])\n",
    "X = pd.DataFrame(X,columns=['x_1','x_2','x_3','y'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>x_1</th>\n",
       "      <th>x_2</th>\n",
       "      <th>x_3</th>\n",
       "      <th>y</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   x_1  x_2  x_3  y\n",
       "0    1    0    0  0\n",
       "1    1    0    1  0\n",
       "2    1    1    0  0\n",
       "3    1    1    1  1"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "X"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = X['y']\n",
    "X = X.drop(columns=['y'])\n",
    "w = [0.03,0.66,0.8]\n",
    "eta = 0.05"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Mi muestra tiene la clase 0 y la salida es 1\n",
      "Se deben de actualizar los pesos [0.03, 0.66, 0.8]\n",
      "Mi muestra tiene la clase 0 y la salida es 1\n",
      "Se deben de actualizar los pesos [-0.02  0.66  0.8 ]\n",
      "Mi muestra tiene la clase 0 y la salida es 1\n",
      "Se deben de actualizar los pesos [-0.07  0.66  0.75]\n",
      "Mi muestra tiene la clase 1 y la salida es 1\n",
      "Se deben de actualizar los pesos [-0.12  0.61  0.75]\n"
     ]
    }
   ],
   "source": [
    "for i,entrada in enumerate(X.values):\n",
    "    z = sum(w*entrada)\n",
    "    y_salida = 0\n",
    "    if z>=0:\n",
    "        y_salida = 1\n",
    "        print(\"Mi muestra tiene la clase\",y.values[i],\"y la salida es\",y_salida)\n",
    "        print(\"Se deben de actualizar los pesos\",w)\n",
    "    else:\n",
    "        print(\"La entrada\",y.values[i],\"es igual a la salida\",y_salida)\n",
    "    if y.values[i] != y_salida:\n",
    "        delta = eta*(y.values[i]-y_salida)\n",
    "        w = w + delta*entrada"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = pd.read_csv('../datasets/CSDMC_API_Train.csv')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = dataset['x']\n",
    "y = dataset['y']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0      LoadLibraryW HeapAlloc HeapAlloc HeapFree Heap...\n",
       "1      RegOpenKeyExW LoadLibraryA GetProcAddress GetP...\n",
       "2      HeapAlloc HeapFree HeapAlloc HeapAlloc HeapFre...\n",
       "3      HeapAlloc HeapFree HeapAlloc HeapAlloc HeapFre...\n",
       "4      HeapAlloc HeapFree HeapAlloc HeapAlloc HeapFre...\n",
       "                             ...                        \n",
       "383    IsBadReadPtr IsBadWritePtr HeapAlloc HeapFree ...\n",
       "384    HeapAlloc GetCurrentThreadId GetDriveTypeA Get...\n",
       "385    HeapAlloc HeapFree HeapAlloc HeapAlloc HeapFre...\n",
       "386    HeapAlloc HeapFree HeapAlloc HeapAlloc HeapFre...\n",
       "387    LoadLibraryW HeapAlloc HeapAlloc HeapFree Heap...\n",
       "Name: x, Length: 388, dtype: object"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "X"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=.2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "vect = Pipeline(steps=[('tfidf',TfidfVectorizer()),\n",
    "                      ('per',Perceptron())])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<style>#sk-container-id-1 {color: black;background-color: white;}#sk-container-id-1 pre{padding: 0;}#sk-container-id-1 div.sk-toggleable {background-color: white;}#sk-container-id-1 label.sk-toggleable__label {cursor: pointer;display: block;width: 100%;margin-bottom: 0;padding: 0.3em;box-sizing: border-box;text-align: center;}#sk-container-id-1 label.sk-toggleable__label-arrow:before {content: \"▸\";float: left;margin-right: 0.25em;color: #696969;}#sk-container-id-1 label.sk-toggleable__label-arrow:hover:before {color: black;}#sk-container-id-1 div.sk-estimator:hover label.sk-toggleable__label-arrow:before {color: black;}#sk-container-id-1 div.sk-toggleable__content {max-height: 0;max-width: 0;overflow: hidden;text-align: left;background-color: #f0f8ff;}#sk-container-id-1 div.sk-toggleable__content pre {margin: 0.2em;color: black;border-radius: 0.25em;background-color: #f0f8ff;}#sk-container-id-1 input.sk-toggleable__control:checked~div.sk-toggleable__content {max-height: 200px;max-width: 100%;overflow: auto;}#sk-container-id-1 input.sk-toggleable__control:checked~label.sk-toggleable__label-arrow:before {content: \"▾\";}#sk-container-id-1 div.sk-estimator input.sk-toggleable__control:checked~label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-1 div.sk-label input.sk-toggleable__control:checked~label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-1 input.sk-hidden--visually {border: 0;clip: rect(1px 1px 1px 1px);clip: rect(1px, 1px, 1px, 1px);height: 1px;margin: -1px;overflow: hidden;padding: 0;position: absolute;width: 1px;}#sk-container-id-1 div.sk-estimator {font-family: monospace;background-color: #f0f8ff;border: 1px dotted black;border-radius: 0.25em;box-sizing: border-box;margin-bottom: 0.5em;}#sk-container-id-1 div.sk-estimator:hover {background-color: #d4ebff;}#sk-container-id-1 div.sk-parallel-item::after {content: \"\";width: 100%;border-bottom: 1px solid gray;flex-grow: 1;}#sk-container-id-1 div.sk-label:hover label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-1 div.sk-serial::before {content: \"\";position: absolute;border-left: 1px solid gray;box-sizing: border-box;top: 0;bottom: 0;left: 50%;z-index: 0;}#sk-container-id-1 div.sk-serial {display: flex;flex-direction: column;align-items: center;background-color: white;padding-right: 0.2em;padding-left: 0.2em;position: relative;}#sk-container-id-1 div.sk-item {position: relative;z-index: 1;}#sk-container-id-1 div.sk-parallel {display: flex;align-items: stretch;justify-content: center;background-color: white;position: relative;}#sk-container-id-1 div.sk-item::before, #sk-container-id-1 div.sk-parallel-item::before {content: \"\";position: absolute;border-left: 1px solid gray;box-sizing: border-box;top: 0;bottom: 0;left: 50%;z-index: -1;}#sk-container-id-1 div.sk-parallel-item {display: flex;flex-direction: column;z-index: 1;position: relative;background-color: white;}#sk-container-id-1 div.sk-parallel-item:first-child::after {align-self: flex-end;width: 50%;}#sk-container-id-1 div.sk-parallel-item:last-child::after {align-self: flex-start;width: 50%;}#sk-container-id-1 div.sk-parallel-item:only-child::after {width: 0;}#sk-container-id-1 div.sk-dashed-wrapped {border: 1px dashed gray;margin: 0 0.4em 0.5em 0.4em;box-sizing: border-box;padding-bottom: 0.4em;background-color: white;}#sk-container-id-1 div.sk-label label {font-family: monospace;font-weight: bold;display: inline-block;line-height: 1.2em;}#sk-container-id-1 div.sk-label-container {text-align: center;}#sk-container-id-1 div.sk-container {/* jupyter's `normalize.less` sets `[hidden] { display: none; }` but bootstrap.min.css set `[hidden] { display: none !important; }` so we also need the `!important` here to be able to override the default hidden behavior on the sphinx rendered scikit-learn.org. See: https://github.com/scikit-learn/scikit-learn/issues/21755 */display: inline-block !important;position: relative;}#sk-container-id-1 div.sk-text-repr-fallback {display: none;}</style><div id=\"sk-container-id-1\" class=\"sk-top-container\"><div class=\"sk-text-repr-fallback\"><pre>Pipeline(steps=[(&#x27;tfidf&#x27;, TfidfVectorizer()), (&#x27;per&#x27;, Perceptron())])</pre><b>In a Jupyter environment, please rerun this cell to show the HTML representation or trust the notebook. <br />On GitHub, the HTML representation is unable to render, please try loading this page with nbviewer.org.</b></div><div class=\"sk-container\" hidden><div class=\"sk-item sk-dashed-wrapped\"><div class=\"sk-label-container\"><div class=\"sk-label sk-toggleable\"><input class=\"sk-toggleable__control sk-hidden--visually\" id=\"sk-estimator-id-1\" type=\"checkbox\" ><label for=\"sk-estimator-id-1\" class=\"sk-toggleable__label sk-toggleable__label-arrow\">Pipeline</label><div class=\"sk-toggleable__content\"><pre>Pipeline(steps=[(&#x27;tfidf&#x27;, TfidfVectorizer()), (&#x27;per&#x27;, Perceptron())])</pre></div></div></div><div class=\"sk-serial\"><div class=\"sk-item\"><div class=\"sk-estimator sk-toggleable\"><input class=\"sk-toggleable__control sk-hidden--visually\" id=\"sk-estimator-id-2\" type=\"checkbox\" ><label for=\"sk-estimator-id-2\" class=\"sk-toggleable__label sk-toggleable__label-arrow\">TfidfVectorizer</label><div class=\"sk-toggleable__content\"><pre>TfidfVectorizer()</pre></div></div></div><div class=\"sk-item\"><div class=\"sk-estimator sk-toggleable\"><input class=\"sk-toggleable__control sk-hidden--visually\" id=\"sk-estimator-id-3\" type=\"checkbox\" ><label for=\"sk-estimator-id-3\" class=\"sk-toggleable__label sk-toggleable__label-arrow\">Perceptron</label><div class=\"sk-toggleable__content\"><pre>Perceptron()</pre></div></div></div></div></div></div></div>"
      ],
      "text/plain": [
       "Pipeline(steps=[('tfidf', TfidfVectorizer()), ('per', Perceptron())])"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "vect.fit(X_train,y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_pred = vect.predict(X_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "85.8974358974359"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "accuracy_score(y_test,y_pred)*100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Regresión\n",
    "\n",
    "La regresión es una medida estadística que ayuda a determinar la relación entre una variable dependiente $\\hat{Y}$, con respecto a una serie variables independientes $X$. La idea es *predecir la variable dependiente* de un conjunto de variables independientes $x_i \\in X$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Cualquier linea puede ser representada de la forma $\\hat{Y} = \\beta X + \\alpha$, donde $\\alpha$ y $\\beta$ son constantes\n",
    "2. El valor de $\\beta$  es denominado la pendiente y determina la dirección y grado de inclinación de la línea\n",
    "3. El valor de $ \\alpha$ es denominado el interceptor en el eje de las $Y$ y determina el punto donde la línea cruza dicho eje\n",
    "4. El término de error $\\epsilon_i$\n",
    "\n",
    "Elementos de la Regresión:\n",
    "![title](Lectura9-6.png)\n",
    "\n",
    "El término de error $\\epsilon_i$, corresponde a la diferente entre la observación y el punto deseado, esto servirá para identificar que tan cercano se encuentra el punto por debajo o encima a la línea $\\hat{Y}$. \n",
    "\n",
    "Existen diferentes medidas de variación para poder calcular el ajuste de  $\\hat{Y}$:\n",
    "\n",
    "1. Suma total de los cuadrados (STC): mida la variación de la variable dependiente $Y$ (punto actual) y el valor predicho $\\hat{Y}$\n",
    "2. Suma de los cuadrados de regresión (SCR): explica la variación atribuible a la relación entre $X$ e $Y$\n",
    "3. Suma de los errores cuadrados (SEC): variación atribuida a los factores predictivos\n",
    "\n",
    "Elementos de la Regresión en términos de la variación:\n",
    "\n",
    "![title](Lectura9-7.png)\n",
    "\n",
    "#### ¿Qué es el coeficiente de determinación?\n",
    "El coeficiente de determinación ($R^2$) es la proporción de la variación total de la variable dependiente que es explicada por la variación de la variable independiente:\n",
    "\\begin{equation}\n",
    "R^2 = \\frac{SCR}{STC}\n",
    "\\end{equation}\n",
    ", donde $0 \\leq R^2 \\leq 1$\n",
    "\n",
    "Valores aproximados de $R^2$:\n",
    "\n",
    "![title](Lectura9-8.png)\n",
    "\n",
    "De lo anterior se observa que el comportamiento es lineal, por lo tanto, este tipo de regresión es denominada *Regresión Lineal*. Sin embargo, dicho tipo de algoritmos son útiles para predecir valores continuos, por ejemplo la <font color='red'>cantidad de dinero en de una transacción fraudulenta de phishing</font>. Sin embargo, <font color='red'>no es útil para para predecir salidas discretas</font>: **¿Es la  muestra malware o no?**\n",
    "\n",
    "#### Regresión Logística \n",
    "\n",
    "La regresión logística es un modelo de clasificación fácil de implementar, ideado principalmente para resolver problemas de clasificación binaria (puede ser extendido a un problema multi-clase si se aplican técnicas como (One-Vs-Rest)).\n",
    "\n",
    "Diferencia entre Regresión Linea y Regresión Logística.\n",
    "\n",
    "![title](Lectura9-9.png)\n",
    "\n",
    "##### Razón de monomios\n",
    "\n",
    "Es la razón de oportunidades de un evento. Es descrito como: \n",
    "\\begin{equation}\n",
    "\\frac{p}{(1-p)}\n",
    "\\end{equation}\n",
    ", dónde $p$ es la probabilidad de un posible evento.\n",
    "\n",
    "De esta forma, tomando en cuenta la razón de monomios, se construye una estructura de regresión probabilista. Es una regresión que <font color='red'> predice la probabilidad de un evento: ¿Qué probabilidad existe de que sea malware o no? </font>\n",
    "\n",
    "\\begin{equation}\n",
    "p(y)=\\frac{1}{1+e^{-(\\beta X + \\alpha)}}\n",
    "\\end{equation}\n",
    "\n",
    "Si la probabilidad estimada es mayor al 50%, entonces el modelo predecirá que la instancia pertenece a la clase (denominada clase positiva, en este caso malware), o en otro caso, predecirá que no lo es (pertenece a la clase negativa, etiquetada como 0)\n",
    "\n",
    "El entrenamiento del algoritmo de Regresión logística, al igual que el Perceptrón, calcula la suma de los pesos de las entradas (más el bias o sesgo), pero su función de activación está basada en el término logístico:\n",
    "\n",
    "\\begin{equation}\n",
    "\\hat{p}= \\sigma(w_0 + \\sum_{\\forall i} w_i \\cdot x_i )\n",
    "\\end{equation}\n",
    "\n",
    ", donde $\\hat{p}$ es la probabilidad de clase y $\\sigma$ es la función logística. Una vez que la Regresión Logística ha estimado la probabilidad $\\hat{p}$ de que una muestra $x$ pertenece a la clase positiva, puede realizar la predicción $\\hat{y}$ de manera más sencilla.\n",
    "\n",
    "\\begin{equation}\n",
    "\\hat{y}=\n",
    "     \\begin{cases}\n",
    "     0 \\text{ si } \\hat{p} < 0 . 5 \\\\\n",
    "     1 \\text{ si }  \\hat{p} \\geq 0 . 5 \\\\\n",
    "      \\end{cases}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import LogisticRegression"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "83.33333333333334"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "vec_lrg = Pipeline(steps=[('tfidf',TfidfVectorizer()),\n",
    "                      ('lrg',LogisticRegression())])\n",
    "vec_lrg.fit(X_train,y_train)\n",
    "y_pred = vec_lrg.predict(X_test)\n",
    "accuracy_score(y_test,y_pred)*100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### ¿por qué el desempeño es menor en comparación con el Perceptrón?\n",
    "\n",
    "Los algoritmos de ML contienen hiperparámetros: parámetros ajustables para controlar el proceso de entrenamiento. Por ejemplo Regresión Logística, puede manipular los pesos que causan alta varianza (regularización)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "85.8974358974359\n",
      "88.46153846153845\n",
      "89.74358974358975\n",
      "89.74358974358975\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/opt/anaconda3/lib/python3.8/site-packages/sklearn/linear_model/_logistic.py:444: ConvergenceWarning: lbfgs failed to converge (status=1):\n",
      "STOP: TOTAL NO. of ITERATIONS REACHED LIMIT.\n",
      "\n",
      "Increase the number of iterations (max_iter) or scale the data as shown in:\n",
      "    https://scikit-learn.org/stable/modules/preprocessing.html\n",
      "Please also refer to the documentation for alternative solver options:\n",
      "    https://scikit-learn.org/stable/modules/linear_model.html#logistic-regression\n",
      "  n_iter_i = _check_optimize_result(\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "91.02564102564102\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/opt/anaconda3/lib/python3.8/site-packages/sklearn/linear_model/_logistic.py:444: ConvergenceWarning: lbfgs failed to converge (status=1):\n",
      "STOP: TOTAL NO. of ITERATIONS REACHED LIMIT.\n",
      "\n",
      "Increase the number of iterations (max_iter) or scale the data as shown in:\n",
      "    https://scikit-learn.org/stable/modules/preprocessing.html\n",
      "Please also refer to the documentation for alternative solver options:\n",
      "    https://scikit-learn.org/stable/modules/linear_model.html#logistic-regression\n",
      "  n_iter_i = _check_optimize_result(\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "91.02564102564102\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/opt/anaconda3/lib/python3.8/site-packages/sklearn/linear_model/_logistic.py:444: ConvergenceWarning: lbfgs failed to converge (status=1):\n",
      "STOP: TOTAL NO. of ITERATIONS REACHED LIMIT.\n",
      "\n",
      "Increase the number of iterations (max_iter) or scale the data as shown in:\n",
      "    https://scikit-learn.org/stable/modules/preprocessing.html\n",
      "Please also refer to the documentation for alternative solver options:\n",
      "    https://scikit-learn.org/stable/modules/linear_model.html#logistic-regression\n",
      "  n_iter_i = _check_optimize_result(\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "92.3076923076923\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/opt/anaconda3/lib/python3.8/site-packages/sklearn/linear_model/_logistic.py:444: ConvergenceWarning: lbfgs failed to converge (status=1):\n",
      "STOP: TOTAL NO. of ITERATIONS REACHED LIMIT.\n",
      "\n",
      "Increase the number of iterations (max_iter) or scale the data as shown in:\n",
      "    https://scikit-learn.org/stable/modules/preprocessing.html\n",
      "Please also refer to the documentation for alternative solver options:\n",
      "    https://scikit-learn.org/stable/modules/linear_model.html#logistic-regression\n",
      "  n_iter_i = _check_optimize_result(\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "92.3076923076923\n"
     ]
    }
   ],
   "source": [
    "#C es el parámetro de regularización\n",
    "for c in range(10,160,20):\n",
    "    vec_lrg = Pipeline(steps=[('tfidf',TfidfVectorizer()),\n",
    "                          ('lrg',LogisticRegression(C=c))])\n",
    "    vec_lrg.fit(X_train,y_train)\n",
    "    y_pred = vec_lrg.predict(X_test)\n",
    "    print(accuracy_score(y_test,y_pred)*100)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
