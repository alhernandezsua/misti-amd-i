{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lectura 9: Entrenamiento de Algoritmos de Aprendizaje Máquina \n",
    "\n",
    "## Aplicaciones de Minería de Datos I\n",
    "### Abril, 2022"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# El cerebro como diseño principal de inteligencia artificial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "En 1943 John Smith et al. crearon el concepto de **célula del cerebro simplificada**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![title](Lectura9-1.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Las neuronas están interconectadas en el cerebro e involucran el **procesamiento** y **transmisión** de señales eléctricas y químicas \t\t\n",
    "2. La neurona  es como una compuerta lógica con **salidas binarias**\n",
    "3. Múltiples señales llegan a la **dentrita**, después se integran al cuerpo celular, si la señal acumulada excede un **umbral**, la señal de salida es transmitida por el **axón**\n",
    "\n",
    "En 1957 Rosenblatt, F. publicó el concepto de **regla de aprendizaje del Perceptrón**. Un algoritmo que aprende de los **coeficientes de peso** más óptimos multiplicados por las **características de entrada** (señales) para tomar una **decisión**. En el contexto de aprendizaje supervisado el algoritmo se resume a : *predecir si una muestra pertenece a una clase o no*\n",
    "\n",
    "![title](Lectura9-2.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Definición formal del Perceptrón\n",
    "\n",
    "\n",
    "1.\tSe define como un problema de clasificación binaria $1$ o $0$\n",
    "2.\tSe define una función de activación $\\phi(z)$\n",
    "3.\tSe tiene una combinación lineal de ciertos valores de entrada $\\mathbf{x}$ y un correspondiente vector de pesos $\\mathbf{w}$, donde $z$ es conocida como la **entrada a la red** ($z = w_{1}x_{1}+ \\dots + w_{m}x_{m}$):\n",
    "\n",
    "\\begin{equation}\n",
    " \\begin{bmatrix} \n",
    "    w_{1} \\\\\n",
    "    \\vdots \\\\\n",
    "\tw_{m} \\\\\n",
    "    \\end{bmatrix},\n",
    "    x = \n",
    " \\begin{bmatrix} \n",
    "    x_{1} \\\\\n",
    "    \\vdots \\\\\n",
    "\tx_{m} \\\\\n",
    "    \\end{bmatrix}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "4. Si la activación de una muestra en particular $x^{(i)}$, esto es, la salida $\\phi(z)$, es mayor que un umbral definido $\\theta$, el resultado predicho es  $1$, de otra forma $-1$\n",
    "5. La función de activación es una  *función de escalón unitario*:\n",
    "\\begin{equation}\n",
    "\\begin{cases}\n",
    "1, \\text{si z}  \\geq \\theta\\\\\n",
    "-1,    \\text{lo demás}    \n",
    "\\end{cases}\n",
    "\\end{equation}\n",
    "6. Si se despeja el umbral y se define un peso  cero como $w_0=-\\theta$ y $x_0=1$, se puede escribir $z$ de una manera más compacta :\n",
    "\\begin{equation}\n",
    "z = w_{0}x_{0} + w_{1}x_{1} + \\dots + w_{m}x_{m} = \\mathbf{w}^T \\mathbf{x}\n",
    "\\end{equation}\n",
    "\\begin{equation}\n",
    "z = \\sum_{j=0}^{m} \\mathbf{x_j}\\mathbf{w_j}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La idea principal de la neurona de MCP y el Perceptrón con umbral de Rosenblatt es utilizar un alcance reducido para emular una neurona individual tal como el cerebro funciona : **dispara una secuencia o no** . En la  siguiente Fig a. se muestra como la entrada a la red es confinada a una salida binaria y en la Fig b. cómo se puede discriminar entre dos clases separables.\n",
    "\n",
    "![title](Lectura9-3.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Regla de actualización de pesos perceptrón\n",
    "\n",
    "1. Iniciar los pesos $\\mathbf{w}$ a $0$ o a números reales pequeños\n",
    "2. Para cada muestra de entrenamiento $\\mathbf{x}^{(i)}$ seguir los siguientes pasos:\n",
    "\n",
    "    a. Calcular el valor de salida $\\hat{y}$\n",
    "\n",
    "    b. Actualizar los pesos\n",
    "3.\tEl valor de salida es la clase predicha por la función de escalón unitario\n",
    "4.\tLa actualización simultanea de cada peso $w_{j}$ en el vector de pesos $\\mathbf{w}$  se escribe como:\n",
    "\n",
    "\\begin{equation}\n",
    "w_{j} := w_{j} + \\Delta w_{j}\n",
    "\\end{equation}\n",
    "5. El valor $\\Delta w_{j}$ que es utilizado para actualizar el peso $w_{j}$, es calculado por la regla de aprendizaje del perceptrón:\n",
    "\n",
    "\\begin{equation}\n",
    "\\Delta w_{j} = \\eta (y^{(i)}-\\hat{y}^{(i)})\n",
    "\\end{equation}\n",
    "\n",
    "En donde:\n",
    "\n",
    "1.\t$\\eta$ es el radio de aprendizaje (constante ente $[0,1]$)\n",
    "2.\t$y^{(i)}$ es la clase verdadera de la \t**i-ésima**  muestra\n",
    "3.\t$\\hat{y}^{(i)}$ es la clase predicha\n",
    "\n",
    "Para un conjunto de datos en  $\\mathbb{R}^2$ se puede realizar el cálculo de actualizaciones de la siguiente manera:\n",
    "\n",
    "1.\t $\\Delta w_{0} = \\eta (y^{(i)}-\\text{salida}^{(i)})$\n",
    "2.\t $\\Delta w_{1} = \\eta (y^{(i)}-\\text{salida}^{(i)})x_1$\n",
    "3.\t $\\Delta w_{2} = \\eta (y^{(i)}-\\text{salida}^{(i)})x_2$\n",
    "\n",
    "Por ejemplo, si el Perceptrón predice la clase correctamente, los pesos permanecen intactos:\n",
    "\n",
    "\\begin{equation}\n",
    "\\Delta w_{j} = \\eta (-1--1)x_{j}^{(j)} = 0 \n",
    "\\end{equation}\n",
    "\n",
    "\\begin{equation}\n",
    "\\Delta w_{j} = \\eta (1-1)x_{j}^{(j)} = 0\n",
    "\\end{equation}\n",
    "\n",
    "La convergencia del Perceptrón está garantizada si se cumplen los siguientes puntos:\n",
    "\n",
    "La dos clases son **linealmente separables**\n",
    "1. El radio de aprendizaje es lo suficientemente pequeño\n",
    "2. Si las dos clases no pueden ser separadas por una frontera de decisión se definen un máximo número de rondas en el conjunto de pruebas $X_{t}$ o un umbral para las clases **mal clasificadas**.\n",
    "\n",
    "En la  siguiente Fig. se muestra un ejemplo de datos linealmente separables y aquellos que no\n",
    "\n",
    "![title](Lectura9-4.png)\n",
    "\n",
    "En la siguiente Fig. se ilustra como el Perceptrón recibe las entradas de una muestra $\\mathbf{x}$ y las combina con sus respectivos pesos $\\mathbf{w}$. La entrada a la red pasa a la función de activación  (función de escalón unitario) y genera la salida binaria:\n",
    "\n",
    "![title](Lectura9-5.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import Perceptron\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.feature_extraction.text import TfidfVectorizer\n",
    "from sklearn.pipeline import Pipeline\n",
    "from sklearn.metrics import accuracy_score\n",
    "import pandas as pd\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = np.array([[1,0,0,0],[1,0,1,0],[1,1,0,0],[1,1,1,1]])\n",
    "X = pd.DataFrame(X,columns=['x_1','x_2','x_3','y'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>x_1</th>\n",
       "      <th>x_2</th>\n",
       "      <th>x_3</th>\n",
       "      <th>y</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   x_1  x_2  x_3  y\n",
       "0    1    0    0  0\n",
       "1    1    0    1  0\n",
       "2    1    1    0  0\n",
       "3    1    1    1  1"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "X"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = X['y']\n",
    "X = X.drop(columns=['y'])\n",
    "w = [0.03,0.66,0.8]\n",
    "eta = 0.05"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Mi muestra tiene la clase 0 y la salida es 1\n",
      "Se deben de actualizar los pesos [0.03, 0.66, 0.8]\n",
      "Mi muestra tiene la clase 0 y la salida es 1\n",
      "Se deben de actualizar los pesos [-0.02  0.66  0.8 ]\n",
      "Mi muestra tiene la clase 0 y la salida es 1\n",
      "Se deben de actualizar los pesos [-0.07  0.66  0.75]\n",
      "Mi muestra tiene la clase 1 y la salida es 1\n",
      "Se deben de actualizar los pesos [-0.12  0.61  0.75]\n"
     ]
    }
   ],
   "source": [
    "for i,entrada in enumerate(X.values):\n",
    "    z = sum(w*entrada)\n",
    "    y_salida = 0\n",
    "    if z>=0:\n",
    "        y_salida = 1\n",
    "        print(\"Mi muestra tiene la clase\",y.values[i],\"y la salida es\",y_salida)\n",
    "        print(\"Se deben de actualizar los pesos\",w)\n",
    "    else:\n",
    "        print(\"La entrada\",y.values[i],\"es igual a la salida\",y_salida)\n",
    "    if y.values[i] != y_salida:\n",
    "        delta = eta*(y.values[i]-y_salida)\n",
    "        w = w + delta*entrada"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "#dataset_malware_es = pd.read_csv('datasets/dataset_malwares.csv')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "#dataset_malware_es['Malware']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "ename": "FileNotFoundError",
     "evalue": "[Errno 2] No such file or directory: 'datasets/CSDMC_API_Train.csv'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mFileNotFoundError\u001b[0m                         Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-23-0e5153cf548f>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mdataset\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mpd\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mread_csv\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'datasets/CSDMC_API_Train.csv'\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m/opt/anaconda3/lib/python3.8/site-packages/pandas/util/_decorators.py\u001b[0m in \u001b[0;36mwrapper\u001b[0;34m(*args, **kwargs)\u001b[0m\n\u001b[1;32m    309\u001b[0m                     \u001b[0mstacklevel\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mstacklevel\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    310\u001b[0m                 )\n\u001b[0;32m--> 311\u001b[0;31m             \u001b[0;32mreturn\u001b[0m \u001b[0mfunc\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m*\u001b[0m\u001b[0margs\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m**\u001b[0m\u001b[0mkwargs\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    312\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    313\u001b[0m         \u001b[0;32mreturn\u001b[0m \u001b[0mwrapper\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/opt/anaconda3/lib/python3.8/site-packages/pandas/io/parsers/readers.py\u001b[0m in \u001b[0;36mread_csv\u001b[0;34m(filepath_or_buffer, sep, delimiter, header, names, index_col, usecols, squeeze, prefix, mangle_dupe_cols, dtype, engine, converters, true_values, false_values, skipinitialspace, skiprows, skipfooter, nrows, na_values, keep_default_na, na_filter, verbose, skip_blank_lines, parse_dates, infer_datetime_format, keep_date_col, date_parser, dayfirst, cache_dates, iterator, chunksize, compression, thousands, decimal, lineterminator, quotechar, quoting, doublequote, escapechar, comment, encoding, encoding_errors, dialect, error_bad_lines, warn_bad_lines, on_bad_lines, delim_whitespace, low_memory, memory_map, float_precision, storage_options)\u001b[0m\n\u001b[1;32m    584\u001b[0m     \u001b[0mkwds\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mupdate\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mkwds_defaults\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    585\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 586\u001b[0;31m     \u001b[0;32mreturn\u001b[0m \u001b[0m_read\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mfilepath_or_buffer\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mkwds\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    587\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    588\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/opt/anaconda3/lib/python3.8/site-packages/pandas/io/parsers/readers.py\u001b[0m in \u001b[0;36m_read\u001b[0;34m(filepath_or_buffer, kwds)\u001b[0m\n\u001b[1;32m    480\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    481\u001b[0m     \u001b[0;31m# Create the parser.\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 482\u001b[0;31m     \u001b[0mparser\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mTextFileReader\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mfilepath_or_buffer\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m**\u001b[0m\u001b[0mkwds\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    483\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    484\u001b[0m     \u001b[0;32mif\u001b[0m \u001b[0mchunksize\u001b[0m \u001b[0;32mor\u001b[0m \u001b[0miterator\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/opt/anaconda3/lib/python3.8/site-packages/pandas/io/parsers/readers.py\u001b[0m in \u001b[0;36m__init__\u001b[0;34m(self, f, engine, **kwds)\u001b[0m\n\u001b[1;32m    809\u001b[0m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0moptions\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;34m\"has_index_names\"\u001b[0m\u001b[0;34m]\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mkwds\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;34m\"has_index_names\"\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    810\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 811\u001b[0;31m         \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_engine\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_make_engine\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mengine\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    812\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    813\u001b[0m     \u001b[0;32mdef\u001b[0m \u001b[0mclose\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/opt/anaconda3/lib/python3.8/site-packages/pandas/io/parsers/readers.py\u001b[0m in \u001b[0;36m_make_engine\u001b[0;34m(self, engine)\u001b[0m\n\u001b[1;32m   1038\u001b[0m             )\n\u001b[1;32m   1039\u001b[0m         \u001b[0;31m# error: Too many arguments for \"ParserBase\"\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m-> 1040\u001b[0;31m         \u001b[0;32mreturn\u001b[0m \u001b[0mmapping\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0mengine\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mf\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m**\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0moptions\u001b[0m\u001b[0;34m)\u001b[0m  \u001b[0;31m# type: ignore[call-arg]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m   1041\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1042\u001b[0m     \u001b[0;32mdef\u001b[0m \u001b[0m_failover_to_python\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/opt/anaconda3/lib/python3.8/site-packages/pandas/io/parsers/c_parser_wrapper.py\u001b[0m in \u001b[0;36m__init__\u001b[0;34m(self, src, **kwds)\u001b[0m\n\u001b[1;32m     49\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     50\u001b[0m         \u001b[0;31m# open handles\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 51\u001b[0;31m         \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_open_handles\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0msrc\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mkwds\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     52\u001b[0m         \u001b[0;32massert\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mhandles\u001b[0m \u001b[0;32mis\u001b[0m \u001b[0;32mnot\u001b[0m \u001b[0;32mNone\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     53\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/opt/anaconda3/lib/python3.8/site-packages/pandas/io/parsers/base_parser.py\u001b[0m in \u001b[0;36m_open_handles\u001b[0;34m(self, src, kwds)\u001b[0m\n\u001b[1;32m    220\u001b[0m         \u001b[0mLet\u001b[0m \u001b[0mthe\u001b[0m \u001b[0mreaders\u001b[0m \u001b[0mopen\u001b[0m \u001b[0mIOHandles\u001b[0m \u001b[0mafter\u001b[0m \u001b[0mthey\u001b[0m \u001b[0mare\u001b[0m \u001b[0mdone\u001b[0m \u001b[0;32mwith\u001b[0m \u001b[0mtheir\u001b[0m \u001b[0mpotential\u001b[0m \u001b[0mraises\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    221\u001b[0m         \"\"\"\n\u001b[0;32m--> 222\u001b[0;31m         self.handles = get_handle(\n\u001b[0m\u001b[1;32m    223\u001b[0m             \u001b[0msrc\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    224\u001b[0m             \u001b[0;34m\"r\"\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/opt/anaconda3/lib/python3.8/site-packages/pandas/io/common.py\u001b[0m in \u001b[0;36mget_handle\u001b[0;34m(path_or_buf, mode, encoding, compression, memory_map, is_text, errors, storage_options)\u001b[0m\n\u001b[1;32m    699\u001b[0m         \u001b[0;32mif\u001b[0m \u001b[0mioargs\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mencoding\u001b[0m \u001b[0;32mand\u001b[0m \u001b[0;34m\"b\"\u001b[0m \u001b[0;32mnot\u001b[0m \u001b[0;32min\u001b[0m \u001b[0mioargs\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mmode\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    700\u001b[0m             \u001b[0;31m# Encoding\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 701\u001b[0;31m             handle = open(\n\u001b[0m\u001b[1;32m    702\u001b[0m                 \u001b[0mhandle\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    703\u001b[0m                 \u001b[0mioargs\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mmode\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mFileNotFoundError\u001b[0m: [Errno 2] No such file or directory: 'datasets/CSDMC_API_Train.csv'"
     ]
    }
   ],
   "source": [
    "dataset = pd.read_csv('datasets/CSDMC_API_Train.csv')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = dataset['x']\n",
    "y = dataset['y']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=.2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "vect = Pipeline(steps=[('tfidf',TfidfVectorizer()),\n",
    "                      ('per',Perceptron())])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'X_train' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-10-0cc25fb1625b>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mvect\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mfit\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mX_train\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0my_train\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m: name 'X_train' is not defined"
     ]
    }
   ],
   "source": [
    "vect.fit(X_train,y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'X_test' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-11-ffbfd3e3747a>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0my_pred\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mvect\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mpredict\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mX_test\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m: name 'X_test' is not defined"
     ]
    }
   ],
   "source": [
    "y_pred = vect.predict(X_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'y_test' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-12-bffc61bc65b6>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0maccuracy_score\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0my_test\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0my_pred\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m*\u001b[0m\u001b[0;36m100\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m: name 'y_test' is not defined"
     ]
    }
   ],
   "source": [
    "accuracy_score(y_test,y_pred)*100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Regresión\n",
    "\n",
    "La regresión es una medida estadística que ayuda a determinar la relación entre una variable dependiente $\\hat{Y}$, con respecto a una serie variables independientes $X$. La idea es *predecir la variable dependiente* de un conjunto de variables independientes $x_i \\in X$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Cualquier linea puede ser representada de la forma $\\hat{Y} = \\beta X + \\alpha$, donde $\\alpha$ y $\\beta$ son constantes\n",
    "2. El valor de $\\beta$  es denominado la pendiente y determina la dirección y grado de inclinación de la línea\n",
    "3. El valor de $ \\alpha$ es denominado el interceptor en el eje de las $Y$ y determina el punto donde la línea cruza dicho eje\n",
    "4. El término de error $\\epsilon_i$\n",
    "\n",
    "Elementos de la Regresión:\n",
    "![title](Lectura9-6.png)\n",
    "\n",
    "El término de error $\\epsilon_i$, corresponde a la diferente entre la observación y el punto deseado, esto servirá para identificar que tan cercano se encuentra el punto por debajo o encima a la línea $\\hat{Y}$. \n",
    "\n",
    "Existen diferentes medidas de variación para poder calcular el ajuste de  $\\hat{Y}$:\n",
    "\n",
    "1. Suma total de los cuadrados (STC): mida la variación de la variable dependiente $Y$ (punto actual) y el valor predicho $\\hat{Y}$\n",
    "2. Suma de los cuadrados de regresión (SCR): explica la variación atribuible a la relación entre $X$ e $Y$\n",
    "3. Suma de los errores cuadrados (SEC): variación atribuida a los factores predictivos\n",
    "\n",
    "Elementos de la Regresión en términos de la variación:\n",
    "\n",
    "![title](Lectura9-7.png)\n",
    "\n",
    "#### ¿Qué es el coeficiente de determinación?\n",
    "El coeficiente de determinación ($R^2$) es la proporción de la variación total de la variable dependiente que es explicada por la variación de la variable independiente:\n",
    "\\begin{equation}\n",
    "R^2 = \\frac{SCR}{STC}\n",
    "\\end{equation}\n",
    ", donde $0 \\leq R^2 \\leq 1$\n",
    "\n",
    "Valores aproximados de $R^2$:\n",
    "\n",
    "![title](Lectura9-8.png)\n",
    "\n",
    "De lo anterior se observa que el comportamiento es lineal, por lo tanto, este tipo de regresión es denominada *Regresión Lineal*. Sin embargo, dicho tipo de algoritmos son útiles para predecir valores continuos, por ejemplo la <font color='red'>cantidad de dinero en de una transacción fraudulenta de phishing</font>. Sin embargo, <font color='red'>no es útil para para predecir salidas discretas</font>: **¿Es la  muestra malware o no?**\n",
    "\n",
    "#### Regresión Logística \n",
    "\n",
    "La regresión logística es un modelo de clasificación fácil de implementar, ideado principalmente para resolver problemas de clasificación binaria (puede ser extendido a un problema multi-clase si se aplican técnicas como (One-Vs-Rest)).\n",
    "\n",
    "Diferencia entre Regresión Linea y Regresión Logística.\n",
    "\n",
    "![title](Lectura9-9.png)\n",
    "\n",
    "##### Razón de monomios\n",
    "\n",
    "Es la razón de oportunidades de un evento. Es descrito como: \n",
    "\\begin{equation}\n",
    "\\frac{p}{(1-p)}\n",
    "\\end{equation}\n",
    ", dónde $p$ es la probabilidad de un posible evento.\n",
    "\n",
    "De esta forma, tomando en cuenta la razón de monomios, se construye una estructura de regresión probabilista. Es una regresión que <font color='red'> predice la probabilidad de un evento: ¿Qué probabilidad existe de que sea malware o no? </font>\n",
    "\n",
    "\\begin{equation}\n",
    "p(y)=\\frac{1}{1+e^{-(\\beta X + \\alpha)}}\n",
    "\\end{equation}\n",
    "\n",
    "Si la probabilidad estimada es mayor al 50%, entonces el modelo predecirá que la instancia pertenece a la clase (denominada clase positiva, en este caso malware), o en otro caso, predecirá que no lo es (pertenece a la clase negativa, etiquetada como 0)\n",
    "\n",
    "El entrenamiento del algoritmo de Regresión logística, al igual que el Perceptrón, calcula la suma de los pesos de las entradas (más el bias o sesgo), pero su función de activación está basada en el término logístico:\n",
    "\n",
    "\\begin{equation}\n",
    "\\hat{p}= \\sigma(w_0 + \\sum_{\\forall i} w_i \\cdot x_i )\n",
    "\\end{equation}\n",
    "\n",
    ", donde $\\hat{p}$ es la probabilidad de clase y $\\sigma$ es la función logística. Una vez que la Regresión Logística ha estimado la probabilidad $\\hat{p}$ de que una muestra $x$ pertenece a la clase positiva, puede realizar la predicción $\\hat{y}$ de manera más sencilla.\n",
    "\n",
    "\\begin{equation}\n",
    "\\hat{y}=\n",
    "     \\begin{cases}\n",
    "     0 \\text{ si } \\hat{p} < 0 . 5 \\\\\n",
    "     1 \\text{ si }  \\hat{p} \\geq 0 . 5 \\\\\n",
    "      \\end{cases}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import LogisticRegression"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'X_train' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-14-b2441712f4cb>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      1\u001b[0m vec_lrg = Pipeline(steps=[('tfidf',TfidfVectorizer()),\n\u001b[1;32m      2\u001b[0m                       ('lrg',LogisticRegression())])\n\u001b[0;32m----> 3\u001b[0;31m \u001b[0mvec_lrg\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mfit\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mX_train\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0my_train\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      4\u001b[0m \u001b[0my_pred\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mvec_lrg\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mpredict\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mX_test\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      5\u001b[0m \u001b[0maccuracy_score\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0my_test\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0my_pred\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m*\u001b[0m\u001b[0;36m100\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mNameError\u001b[0m: name 'X_train' is not defined"
     ]
    }
   ],
   "source": [
    "vec_lrg = Pipeline(steps=[('tfidf',TfidfVectorizer()),\n",
    "                      ('lrg',LogisticRegression())])\n",
    "vec_lrg.fit(X_train,y_train)\n",
    "y_pred = vec_lrg.predict(X_test)\n",
    "accuracy_score(y_test,y_pred)*100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### ¿por qué el desempeño es menor en comparación con el Perceptrón?\n",
    "\n",
    "Los algoritmos de ML contienen hiperparámetros: parámetros ajustables para controlar el proceso de entrenamiento. Por ejemplo Regresión Logística, puede manipular los pesos que causan alta varianza (regularización)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'X_train' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-15-be34daac8802>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      3\u001b[0m     vec_lrg = Pipeline(steps=[('tfidf',TfidfVectorizer()),\n\u001b[1;32m      4\u001b[0m                           ('lrg',LogisticRegression(C=c))])\n\u001b[0;32m----> 5\u001b[0;31m     \u001b[0mvec_lrg\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mfit\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mX_train\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0my_train\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      6\u001b[0m     \u001b[0my_pred\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mvec_lrg\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mpredict\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mX_test\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      7\u001b[0m     \u001b[0mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0maccuracy_score\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0my_test\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0my_pred\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m*\u001b[0m\u001b[0;36m100\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mNameError\u001b[0m: name 'X_train' is not defined"
     ]
    }
   ],
   "source": [
    "#C es el parámetro de regularización\n",
    "for c in range(10,160,20):\n",
    "    vec_lrg = Pipeline(steps=[('tfidf',TfidfVectorizer()),\n",
    "                          ('lrg',LogisticRegression(C=c))])\n",
    "    vec_lrg.fit(X_train,y_train)\n",
    "    y_pred = vec_lrg.predict(X_test)\n",
    "    print(accuracy_score(y_test,y_pred)*100)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
